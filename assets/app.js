import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
import App from './App.vue'
import Components from 'components/_index'

// Firebase stuff
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

import { createStore } from 'store/index'
import { createRouter } from 'router/index'
import { sync } from 'vuex-router-sync'

Vue.use(Vuetify)

// Firebase stuff
Vue.use(VueFire)
firebase.initializeApp({
  apiKey: "AIzaSyBQ8Z5O1gh6lAZa796EBZLwQPvFju24ImA",
  authDomain: "notes-ebdd2.firebaseapp.com",
  databaseURL: "https://notes-ebdd2.firebaseio.com",
  projectId: "notes-ebdd2",
  storageBucket: "notes-ebdd2.appspot.com",
  messagingSenderId: "772590461535"
})
export const db = firebase.firestore()

Object.keys(Components).forEach(key => {
  Vue.component(key, Components[key])
})

// Expose a factory function that creates a fresh set of store, router,
// app instances on each call (which is called for each SSR request)
export function createApp (ssrContext) {
  // create store and router instances
  const store = createStore()
  const router = createRouter()

  // sync the router with the vuex store.
  // this registers `store.state.route`
  sync(store, router)

  // create the app instance.
  // here we inject the router, store and ssr context to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = new Vue({
    router,
    store,
    ssrContext,
    render: h => h(App)
  })

  // expose the app, the router and the store.
  // note we are not mounting the app here, since bootstrapping will be
  // different depending on whether we are in a browser or on the server.
  return { app, router, store, db }
}
